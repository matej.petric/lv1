﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ConsoleApp1
{
    class Note
    {
        private string text;
        private string author;
        private int priority;

        public string getText() { return text; }
        public string getAuthor() { return author; }
        public int getPriority() { return priority; }
        public void setText(string Text) { text = Text; }
        public void setPriority(int Priority)
        {   
            if(Priority<=0)
            {
                priority = 1;
            }
            else if(Priority>5)
            {
                priority = 5;
            }
            else
            {
                priority = Priority;
            }           
        }

        public Note() { text = "blank"; author = "unknown"; priority = 5; }
        public Note(string Text, string Author, int Priority) { text = Text; author = Author; setPriority(Priority); }
        public Note(string Text, int Priority) { text = Text; author = "unknown"; setPriority(Priority); }

        public string Text { get { return text; } private set { text = value; } }
        public string Author { get { return author; } }
        public int Priority { get { return priority; } private set { setPriority(value); } }
        public override string ToString()
        {
            return text + " - " + author + " - " + priority;
        }
    }

    class TimeNote : Note
    {
        private DateTime time;

        public TimeNote() { time = DateTime.Now; }
        public TimeNote(string Text, string Author, int Priority) : base(Text, Author, Priority) { time = DateTime.Now; }
        public DateTime Time { get { return time; } set { time = value; } }
        public override string ToString()
        {
            return base.ToString() + ", " + time;
        }
    }
}
