﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ToDoList
    {
        
        private List<Note> notes;

        public ToDoList() { notes = new List<Note>(); }
        public void AddNote(Note newNote) { notes.Add(newNote); }
        public int getHighestPriority()
        {
            int highestPriority = notes[0].Priority;
            foreach (Note note in notes)
            {
                if (highestPriority > note.Priority)
                {
                    highestPriority = note.Priority;
                }
            }
            return highestPriority;
        }
        public void RemoveHighestPriorityNotes()
        {
            int highestPriority = getHighestPriority();
            foreach (Note note in notes.ToList())
            {
                if (note.Priority == highestPriority)
                {
                    notes.Remove(note);
                }
            }
        }
        public void RemoveAllNotes()
        {
            notes.Clear();
        }
        public Note getNote(int index)
        {
            return notes[index];
        }
        public override string ToString()
        {
            StringBuilder listPrintFormat = new StringBuilder("\n");
            foreach (Note note in notes)
            {
                listPrintFormat.Append(note + "\n");
            }
            return listPrintFormat.ToString();
        }
    }
}

