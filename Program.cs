﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {            

            Note a = new Note("A", "I", 5);
            Note b = new Note("B", "You", 14);
            Note c = new Note("C", "He", 3);

            Console.WriteLine(a + ", " + b + ", " + c);

            Note d = new Note("D", "She", 1);
            Note e = new Note("E", "It", -22);
            Note f = new Note("F", 3);

            TimeNote myTimeNote = new TimeNote("RPOON", "Me", 1);
            Console.WriteLine(myTimeNote.ToString());

            ToDoList myToDoList = new ToDoList();
            myToDoList.AddNote(a);
            myToDoList.AddNote(b);
            myToDoList.AddNote(c);
            myToDoList.AddNote(d);
            myToDoList.AddNote(e);
            myToDoList.AddNote(f);

            Console.WriteLine(myToDoList);

            myToDoList.RemoveHighestPriorityNotes();


            Console.WriteLine(myToDoList);

        }
    }
}
